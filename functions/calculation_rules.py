"""
File consists of several functions for the calculation rules of FAIR Quality KPIs
"""

from functions.classes import *

def test_function():
    """Test function to check module functionality"""
    print("You called the test function.")

# MASSE
def kpi_mass(system: LegoAssembly)->float:
    """
    Calculates the total mass of the system

    Args:
        system (LegoAssembly): LegoAssembly object that represents the system

    Returns:
       total_mass (float): Sum of masses of all components in the system in g

    Raises:
        TypeError: If an argument with unsupported type is passed
            (i.e. anything other than LegoAssembly).
   """
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to kpi_mass()")
    total_mass = 0
    for c in system.get_component_list(-1):
        total_mass += c.properties["mass [g]"]
    return total_mass # alternative: sum(c.properties["mass [g]"] for c in system.get_component_list(-1))

"""
# Add new functions for calculating metrics

# AUFWAND    
def kpi_aufwand(system: LegoAssembly)->float:
    """
    Calculates the total expense associated with the system.

    Args:
        system (LegoAssembly): LegoAssembly object representing the system.

    Returns:
        total_aufwand (float): Total expense associated with the system.

    Raises:
        TypeError: If an argument with unsupported type is passed (other than LegoAssembly).
    """
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to kpi_aufwand()")
    total_aufwand = 0
    for c in system.get_component_list(-1):
        total_aufwand += c.properties["price [Euro]"]
    return total_aufwand # alternative: sum(c.properties["mass [g]"] for c in system.get_component_list(-1))


# VERFUEGBARKEIT
def kpi_verfuegbarkeit(system: LegoAssembly) -> float:
   
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to kpi_verfuegbarkeit()")

    availability = 0  
    for c in system.get_component_list(-1):
        availability += c.properties["delivery time [days]"]
    return availability
  

# AKZEPTANZ
def kpi_akzeptanz(system: LegoAssembly) -> float:
   
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to kpi_akzeptanz()")

    acceptance = 0 
    for c in system.get_component_list(-1):
        acceptance += c.properties["diameter [mm]"]
    return acceptance
"""


if __name__ == "__main__":
    """
    Function to inform that functions in this module is
    intended for import not usage as script
    """
    print(
        "This script contains functions for calculating the FAIR Quality KPIs."
        "It is not to be executed independently."
    )
